/**
	\author Garnier Cl�ment
	\brief Definitions of library elements
*/
#include "SimpleStringTemplater.h"
#include <algorithm>
#include <sstream>

SimpleStringTemplater::SimpleStringTemplater(std::string path, unsigned char idSeparator, unsigned char langSeparator, unsigned char varSeparator):
	_idSeparator(idSeparator),
	_langSeperator(langSeparator),
	_varSeparator(varSeparator),
	_templates(new std::map<std::string, StringTemplate>())
{
	if(!_templates) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return;
	}
	LoadFile(path);
}

SimpleStringTemplater::~SimpleStringTemplater()
{
	if (_templates) {
		delete _templates;
		_templates = nullptr;
	}

	if (_defaultLang) {
		delete _defaultLang;
		_defaultLang = nullptr;
	}
}

void SimpleStringTemplater::LoadFile(std::string path)
{
	if(!_templates) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return;
	}
	if (path.empty())	return;
	
	std::ifstream stream(path);
	if (!stream.is_open())	return;

	std::string content;
	content.assign(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());

	stream.close();

	LoadString(content);
}

void SimpleStringTemplater::LoadString(std::string content)
{
	if (!_templates) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return;
	}
	if (content.empty())	return;

	std::stringstream stream(content);

	uint32_t lineIndex = 0;
	std::string line;
	std::string templateId;
	std::string curId = "";
	std::string tmpKey = "";
	std::string tmpValue = "";



	while (std::getline(stream, line)) {
		lineIndex++;

		if (line.empty())	continue;

		auto langSepPos = line.find(_langSeperator);	// Check if line contains a language separator

		if (langSepPos == std::string::npos) {	// If lang separator not found
			auto tempKeyPos = line.find("TemplateId");
			if (tempKeyPos == std::string::npos)
				continue;

			// Find template id
			std::string tempIdInfo = line.substr(tempKeyPos);

			auto firstIdSeparator = tempIdInfo.find_first_of(_idSeparator);
			auto lastIdSeparator = tempIdInfo.find_last_of(_idSeparator);

			std::string id = tempIdInfo.substr(firstIdSeparator + 1, lastIdSeparator - firstIdSeparator - 1);

			// Check if id is valid
			if (id.empty()) {
#ifdef _DEBUG	
				std::cerr << "[SimpleStringTemplater] Error: template id empty (" << lineIndex << ":" << tempKeyPos << ")" << std::endl;
#endif
				continue;
			}
			auto nextNotVarChar = std::find_if_not(id.begin(), id.end(), STRING_TEMPLATE_VAR_CHARACTER_TEST);
			if (nextNotVarChar != id.end()) {
#ifdef _DEBUG
				std::cerr << "[SimpleStringTemplater] Error: template id contains a non-valid character (" << lineIndex << ":" << (tempKeyPos + std::distance(id.begin(), nextNotVarChar) + 1) << ")" << std::endl;
#endif
				continue;
			}
			curId = id;
		}
		else {		// If lang separator found
			std::string langKey = line.substr(0, langSepPos);	// Get language key

			if (langKey.empty()) {
#ifdef _DEBUG	
				std::cerr << "[SimpleStringTemplater] Error: lang key empty (" << lineIndex << ")" << std::endl;
#endif			
				continue;
			}

			std::string::iterator tmpIt;
			if ((tmpIt = std::find_if_not(langKey.begin(), langKey.end(), STRING_TEMPLATE_VAR_CHARACTER_TEST)) != langKey.end()) {
#ifdef _DEBUG	
				std::cerr << "[SimpleStringTemplater] Error: lang key contains a non-valid character (" << lineIndex << ":" << (std::distance(langKey.begin(), tmpIt) + 1) << ")" << std::endl;
#endif			
				continue;
			}

			tmpKey = curId + langKey;

			if (_templates->count(tmpKey) > 0) {
#ifdef _DEBUG
				std::cerr << "[SimpleStringTemplater] Warning: the template " << curId << " has multiple template strings for the language " << langKey << " (" << lineIndex << ")" << std::endl;
#endif
				continue;
			}

			tmpValue = line.substr(langSepPos + 1);

			while (std::isspace(tmpValue[0]))
				tmpValue = tmpValue.erase(0, 1);

#ifdef _DEBUG
			std::cout << "[SimpleStringTemplater] Template " << curId << "-" << langKey << " resgistered : " << tmpValue << std::endl;
#endif

			(*_templates)[tmpKey] = StringTemplate(tmpValue, _varSeparator);
		}
	}

	stream.str("");
}

StringTemplate SimpleStringTemplater::Find(std::string templateId, std::string lang)
{
	if(!_templates) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return StringTemplate();
	}
	try {
		if (lang.empty() && _defaultLang)
			lang = *_defaultLang;
		return _templates->at(templateId + lang);
	}
	catch (const std::out_of_range&) {
		return StringTemplate();
	}
}

StringTemplate SimpleStringTemplater::operator()(const std::string templateId, const std::string lang)
{
	if(!_templates) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return StringTemplate();
	}
	return Find(templateId, lang);
}

void SimpleStringTemplater::SetDefaultLanguage(const std::string languageCode)
{
	if (_defaultLang)
		delete _defaultLang;
	_defaultLang = new std::string(languageCode);
}

StringTemplate::StringTemplate(std::string templateString, unsigned char varSeparator): 
	_template(new std::string(templateString)),
	_variables(new std::vector<std::string>()),
	_varSeparator(varSeparator)
{
	if (!_template || !_variables) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return;
	}

	std::string::iterator nextSeparator;
	std::string tmp;

	auto end = _template->end();	// End of the template string
	// Position of the next variable of the template (including separators)
	auto nextVar = std::find(_template->begin(), end, _varSeparator);
	
	do {
		for(; nextVar != end; nextVar++) {	// Search over the whole template string
			if (*nextVar != _varSeparator)	continue;					// If there is a seperator

			// Find next non-alphanumeric character and next variable seperator
			auto nextAlNum = std::find_if_not(nextVar + 1, end, STRING_TEMPLATE_VAR_CHARACTER_TEST);
			nextSeparator = std::find(nextVar + 1, end, _varSeparator);

			if (nextAlNum != nextSeparator) {	// If next non-alphanumeric character and the separator are not the same
				nextVar = nextSeparator - 1;	// One step backward (to avoid skipping one character)
				continue;
			}

			if (nextSeparator == end)	continue;	// Check if a seperator found
			if (nextSeparator == nextVar + 1)	continue;	// Check if there is a variable name
		
			break;	// Then, process
		}

		if (nextVar != end) {	// Check if variable found
			tmp = std::string(nextVar + 1, nextSeparator);	// Retrieve variable name
			//tmp.pos = std::distance(_template.begin(), nextVar);	// Get position by retrieving iterator index 
			_variables->push_back(tmp);
			nextVar = nextSeparator + 1;		// Skip variable name and separators
			end = _template->end();	// Update end of the template string
		}

		if (nextVar == end)	break;
	} while (true);

	auto unique = std::unique(_variables->begin(), _variables->end());
	if (unique != _variables->end())
		_variables->erase(unique);
}

StringTemplate::StringTemplate(const StringTemplate& other) :
	_varSeparator(other._varSeparator),
	_variables(new std::vector<std::string>()),
	_template(new std::string())
{
	if (other._variables) {
		auto it = other._variables->begin();
		while (it != other._variables->end())
			_variables->push_back(*it++);
	}

	if (other._template) {
		*_template = *other._template;
	}
}

StringTemplate::~StringTemplate()
{
	if (_variables) {
		delete _variables;
		_variables = nullptr;
	}
	if (_template) {
		delete _template;
		_template = nullptr;
	}
}

bool StringTemplate::IsReady() const
{
	if(!_variables || !_template) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return false;
	}
	return _variables->empty();
}

StringTemplate StringTemplate::Replace(std::string varName, std::string value)
{
	if (!_template || !_variables) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return StringTemplate();
	}

	std::string replaced = *_template;
	uint32_t pos, size, offset = 0, count = 0;
	while (__LocalizeVariable(varName, pos, size, offset)) {
		count++;
		offset = pos + 1;
		replaced = _template->replace(pos, size, value);
	}

	if (count <= 0)	return *this;

	StringTemplate res(*this);
	auto remove = std::remove(res._variables->begin(), res._variables->end(), varName);
	if (remove != res._variables->end())
		res._variables->erase(remove);

	return res;
}

std::string StringTemplate::Terminate(std::string value)
{
	if(!_template || !_variables) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return std::string();
	}

	StringTemplate tmp(*this);
	std::for_each(_variables->begin(), _variables->end(), [&](std::string varName) {
		tmp = tmp.Replace(varName, value);
	});

	return tmp.Str();
}

std::string StringTemplate::Str() const
{
	return *_template;
}

StringTemplate& StringTemplate::operator=(const StringTemplate& other)
{
	if(!other._template || !other._variables) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return *this;
	}
	delete _template;
	delete _variables;
	_variables = new std::vector<std::string>(*other._variables);
	_template = new std::string(*other._template);
	return *this;
}

bool StringTemplate::__LocalizeVariable(std::string varName, uint32_t& pos, uint32_t& size, uint32_t offset)
{
	if(!_template || !_variables) {
		std::cerr << "[SimpleStringTemplater] CRITICAL: The library is imcompatible with your system" << std::endl;
		return false;
	}

	if (varName.empty()) {
		pos = 0;
		size = 0;
		return false;
	}

	auto varIt = std::find(_variables->begin(), _variables->end(), varName);
	if (varIt == _variables->end()) {
		pos = 0;
		size = 0;
		return false;
	}

	//Construct pattern with separators
	std::string pattern = static_cast<char>(_varSeparator) + varName + static_cast<char>(_varSeparator);


	// Look for variable pattern
	auto start = __FindPattern(*_template, pattern);
	if (start == _template->end()) {
		pos = 0;
		size = 0;
		return false;
	}

	// Look for the end of the pattern
	auto end = std::find(start + 1, _template->end(), _varSeparator);	// Get 
	if (end == _template->end()) {
		pos = 0;
		size = 0;
		return false;
	}

	// Retrieve position and size from iterators
	pos = std::distance(_template->begin(), start);
	size = std::distance(start, end + 1);
	return true;
}

std::string::iterator StringTemplate::__FindPattern(std::string& src, std::string pattern)
{
	size_t patternSize = pattern.size();

	if (patternSize > src.size())	return src.end();	// Check if pattern can fit in src string

	std::string::iterator res = src.begin(), edge = res + patternSize, tmp, pIt;
	do {
		for (tmp = res, pIt = pattern.begin(); tmp != edge; tmp++, pIt++)	// Look for a match
			if (*tmp != *pIt)	
				break;	// Once a character is different, breaks

		if (pIt == pattern.end())	break;	// If found a match, breaks

		if (edge == src.end()) {	// If not found
			res = src.end();		// adjust result
			break;
		}
		res++;	edge++;
	} while (true);

	return res;
}