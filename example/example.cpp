#include "SimpleStringTemplater.h"

int main(int argc, char*** argv) {

	//------------------ LOAD STRING USAGE ----------------------
	StringTemplate st("My name is :name: and I'm :age: years old. I live in :location:. My favorite hobbiy is :hobby:.");
	
	auto res = st.Replace("name", "Clement")
		.Replace("age", "20")
		.Replace("location", "France")
		.Terminate();
	
	std::cout << res << std::endl;
	// prints "My name is Clement and I'm 20 years old. Il live in France. My favorite hobby is ?."

	//----------------- LOAD FILE USAGE -------------------------

	SimpleStringTemplater sst("templates.txt");
	auto r = sst.Find("presentation", "en").Replace("name", "Clement");

	std::cout << r.Str() << std::endl;
	// prints "My name is Clement"

	return 0;
}