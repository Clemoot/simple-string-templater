# SimpleStringTemplater Library

The library is an simple C++ templater with an easy usage. It is easy to setup.

It adds two classes : `StringTemplate` and `SimpleStringTemplater`

The `StringTemplate` class is scalable and can be reusable freely. It handles each template on its own.
The `SimpleStringTemplater` is designed to store many sentences in different languages. 
It can read template files (format explained in part `Creating a template file`).
These files are easy to understand. The templater can also directly load strings if you have to encrypt your template files.

**WARNING:** the build files were compiled with Visual Studio 2019 (v142).

## Creating a template

To create a template, you simply write a sentence with some words wrapped with two colons `:` (like this `:var:`). 
Colons are the default variable separator. It can be changed with the constructor prototype. 
However, the separator must be a non-alphanumeric character. Every alphanumeric character and the underscore are reserved for variable name.

Template example: `Hello ! My name is :name:.`
Here, `name` is the only variable in the template.

A malformed template would be `Hello ! My name is :name :.`, because space is not an alphanumeric character.

If the template is malformed, it will not be considered.

Each template can have an infinite number of variables and several variables can have the same name. 
In this case, all the variables will have the same value.

## Creating a template file

The template file aims to store a sentence in different languages. Each template are identified with an id and a language code. 

To set the id of a template, you must write the line

`TemplateId "id"`

The keyword `TemplateId` is mandatory and the id must be wrapped with quotes. The quotes can be changed in any other non-alphanumerical character.

Once the id set, you can add templates and its language code by using the format :

`lang_code> Here is the :template:`

`>` is reserved to seperate the language code and the template.
It can be changed in the constructor of the class with another non-alphanumerical character.
However, once the first 'greater than' character is passed, you can use it in your template.
The language code has same character restricter of variables in templates : it can only use alpha-numerical character and underscore. 
The template do not have any limit size.

**WARNING**: The algorithm reading these files is *"line-sensitive"*

A well-formed file example :
```
TemplateId "presentation"
en> My name is :name:
fr> Mon nom est :name:
es> Mi nombre es :name:

TemplateId "living_place"
en> I live in :place:
deutsch> Ich :verb: in :place:
```

Each template category do not need to have same languages and the same variables. Each template can be handled like you want programmatically.

## `SimpleStringTemplater` usage

To start using the templater, you simply have to construct it. Here is its constructor :

`SimpleStringTemplater(std::string path = "", unsigned char idSeperator = '\"', unsigned char langSeparator = '>', unsigned char varSeparator = ':')`

The `path` parameter allows you to read a file on construct.

Once it is construct, you can load files or strings by using the members :

`void SimpleStringTemplater::LoadFile(std::string path)`
`void SimpleStringTemplater::LoadString(std::string content)`

To retrieve the template of your template file, you should use the member `Find`:

`StringTemplate SimpleStringTemplater::Find(std::string templateId, std::string lang)`

`templateId` and `lang` are simply the template id and the language code written in the template file.
If the templater do not find any template, it would return an empty template.

An alias to `Find` is created with `operator()`, so `sst.Find("presentation", "en")` is equivalent to `sst("presentation", "en")`.

## `StringTemplate` usage

This class handles every template string. To initiliaze a template, you must specify the template string in its constructor :

`StringTemplate(std::string templateString = "", unsigned char varSeparator = ':')`

As well as the templater, the separator can be configured.

On construct, it will analyze the string and point out each variable of the string 
to assure a fast response in case when the variable you want is not in the template.

The function `bool IsReady()` informs you, whether or not, the template do not have anymore variables.
Even if the template is not ready you can retrieve the current template string with the member `std::string Str()`.

To replace a variable with a value, you should use the function `StringTemplate Replace(std::string varName, std::string value)`.
It returns a new StringTemplate object with the modification on the template string, so you can chain the `Replace` function and control the state of the template.

In some case, youc an use the function `std::string Terminate(std::string value = "?")` which replaces all remaining variables by specified value.

---

You can find an example in the `example` folder with a basic usage of the library. 